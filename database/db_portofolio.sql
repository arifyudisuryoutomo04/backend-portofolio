-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Sep 2022 pada 11.57
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_portofolio`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Experience', 'experience.', '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(2, 'Portfolio', 'portfolio', '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(3, 'Achivement', 'achivement', '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(4, 'Activity', 'activity', '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(5, 'Blog', 'blog', '2022-09-16 16:46:30', '2022-09-16 16:46:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_09_16_231823_create_posts_table', 1),
(6, '2022_09_16_231919_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `category_id`, `title`, `slug`, `image`, `excerpt`, `body`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 'Ab cupiditate amet.', 'sint-quasi-corporis-molestiae-ex-dolores', NULL, 'Vitae et blanditiis laboriosam quo et dolores. Mollitia consectetur soluta itaque quasi quos. Alias recusandae vitae sit enim soluta ut aperiam.', '<p>Nam deleniti in adipisci adipisci molestias exercitationem nulla. Veritatis doloremque consequatur maiores aut accusamus eos. Dolorem velit quo ut quae eum et.</p><p>Fuga veniam vel cum illo. Unde molestiae ea at eius est cumque eos. Neque qui voluptas qui aliquam.</p><p>Ipsa ratione aliquam magni ipsum inventore fugit alias perferendis. Quia quos tempore non necessitatibus doloremque laborum quis. Mollitia deserunt error est doloribus et. Eveniet quas distinctio molestiae neque voluptatem quo.</p><p>Eius sequi quia qui blanditiis. Fuga culpa vel earum optio tempora. Libero et excepturi ex nisi quo.</p><p>Voluptas iusto dolores sit sed. In nobis provident eveniet repellat. Et libero impedit facilis.</p><p>Incidunt ea at vero totam. Omnis eum deserunt similique consectetur voluptatem non iste. Quo eaque ea ducimus quis aliquam dolorem non fugit. Atque quo eius delectus tempore enim.</p><p>Dignissimos aperiam voluptatem distinctio voluptatem dolorum repellat a. Inventore voluptatem quos corporis. Ad autem excepturi facilis quaerat blanditiis quidem. Ut omnis dicta quia ipsa sint.</p><p>Nisi sit eum rerum totam. Est voluptatem earum veritatis consequatur. Illum ullam eum enim.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(2, 1, 1, 'Qui at eos cumque at.', 'neque-illum-magni-aut-est-culpa', NULL, 'Ut aut quidem sit minus rerum. Ipsam voluptatibus officia aut autem. Veniam voluptatibus inventore aut animi rerum. Ut qui accusantium velit sint enim temporibus.', '<p>Ipsam aut sint vel est nisi molestias. Odit laudantium quisquam debitis aliquid asperiores modi laborum. Adipisci provident quidem inventore.</p><p>Odio rem quia consequatur est repellendus exercitationem voluptatum. Neque exercitationem at aut. Omnis eos dolor voluptas nisi tempora sed est. Alias deleniti iure odio soluta sunt est inventore.</p><p>Tenetur cupiditate voluptatem voluptatum dolorem odit neque est. Dolore deserunt officia culpa minima dolores id voluptatum dolores. Laborum et at hic illo.</p><p>Similique quia perspiciatis dolor odio repellendus iusto. Esse dolores aut harum odio inventore quidem iusto. Consectetur sed ab sint accusamus omnis.</p><p>Ipsa aliquam amet ratione fugiat vel. Et repudiandae mollitia sed omnis nulla architecto voluptate. Saepe aliquam provident est voluptatem placeat et. Corporis ullam pariatur voluptates dicta ut.</p><p>Error eos consectetur cum vel qui cumque a. Facilis esse consequatur fugiat in dolore nam hic. Exercitationem a dolorum quia aut voluptatibus.</p><p>Consequatur voluptas facere suscipit ut eos in rerum. Odio quidem sunt aut omnis et aut et. Consequuntur et aut assumenda ipsa at et dicta dolore. Reprehenderit qui facere qui.</p><p>Qui cum sequi suscipit rem molestiae molestiae voluptas. Mollitia totam tempora officiis alias vero ratione. Odit praesentium occaecati at nam. Suscipit voluptatem est non repudiandae ipsa ut eum.</p><p>Dolore enim delectus distinctio rerum. Et explicabo ad a quia quisquam. Assumenda facere est distinctio ut. Consequuntur illo sapiente reiciendis id voluptatum.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(3, 2, 5, 'Est vero sapiente.', 'voluptatem-ducimus-nesciunt-est-nostrum', NULL, 'Quis in autem natus et sed in similique. Quo quo est voluptas inventore ipsam similique est quidem. Dolor voluptate repudiandae dicta non porro dolorum.', '<p>Et consectetur nostrum autem. Alias id sunt quia qui eos. Illum voluptatem nobis sunt.</p><p>Et tenetur esse adipisci totam et sit. Quia sunt et repudiandae quia enim. Et itaque saepe iure repudiandae ea. Dolor aut et at at non est aliquam.</p><p>Doloremque culpa ducimus eveniet. Neque nihil voluptas odit ullam ut. Deleniti sed cum id enim ut. Ducimus temporibus incidunt velit voluptas.</p><p>Eos dolores quasi sequi illo animi et. Sed esse numquam facilis et inventore sed atque nostrum. Ipsam dolorum alias tempore consequuntur explicabo quis. Est eos nihil deserunt illum.</p><p>Perspiciatis et nihil harum soluta pariatur cupiditate adipisci. Quia maxime est possimus eligendi. Ipsa sint consequatur et molestiae rerum in.</p><p>Enim suscipit adipisci voluptas qui consequatur. Cumque est id est dolores nemo eos. Laboriosam ut qui quo cupiditate quia amet rerum et. Iure veniam ratione maxime aspernatur repellendus animi.</p><p>Veniam doloribus in iste quod rem. Numquam nihil dolor et. Quia et ipsum eos velit non.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(4, 3, 2, 'Magni laudantium a adipisci est vero et quam.', 'quod-explicabo-qui-eum-odio', NULL, 'Consequatur blanditiis nisi dolores beatae qui consectetur. Exercitationem a ut assumenda porro. Molestiae quo aut repellat cupiditate inventore odio. Molestias nesciunt et et alias eum.', '<p>In assumenda aut dignissimos. Exercitationem et dolores modi est voluptates. Deserunt asperiores voluptas quia sunt dolore eum.</p><p>Autem accusantium possimus ut et amet reiciendis quam. Molestiae ut velit aut quis ea. Veritatis esse voluptatem perferendis quibusdam dolorem. Eum omnis veritatis nihil ex voluptatem.</p><p>Occaecati quae suscipit autem consequatur deserunt ut. Aspernatur sint impedit perferendis consectetur assumenda. Ea et eum beatae. Quis saepe possimus consequuntur facere sint vitae.</p><p>Debitis cum porro laborum asperiores. Omnis est odio tempora aut qui quam. Voluptate consequatur natus et doloribus cum. Et sequi optio distinctio veritatis.</p><p>Ipsam est dolorem magnam eius culpa quia ea. Accusantium alias aspernatur nisi a. Dolore esse ratione et distinctio. Praesentium aliquid sint dolorem iste perspiciatis non modi nihil.</p><p>Eum iure reiciendis in nihil doloribus. Ratione suscipit nostrum ea ut. Ex quos officia alias voluptas eos consequatur iure.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(5, 3, 2, 'Aspernatur illum alias.', 'libero-esse-molestiae-ut-minus', NULL, 'Et dolor dolore cupiditate mollitia molestiae eum qui. Eum quae libero quo voluptas doloremque est accusamus.', '<p>Quia nulla quam laborum ratione corporis nostrum. Molestiae rerum rem natus molestiae.</p><p>Ipsa soluta inventore recusandae quam fugiat ab. Labore repudiandae voluptas inventore ducimus nam. Earum rerum et quidem consequatur dolore ratione minus. Et ea libero velit aliquid blanditiis omnis.</p><p>Est velit quae fugiat eum. Aliquam dicta placeat vel numquam voluptatum et rem. Ducimus sit id facilis rerum a.</p><p>Quia voluptate dolores excepturi voluptatem. Qui quis a quos amet recusandae ratione qui. Magni officiis ea quasi maxime ut expedita minima. Asperiores quia rerum similique explicabo ut.</p><p>Doloremque corrupti voluptas voluptatem nesciunt. Accusantium aut voluptates iste voluptatibus voluptates omnis nobis. Reprehenderit aut voluptates quis saepe laborum dolores ab quam.</p><p>In earum aut voluptates fugit aliquid. Ut assumenda at qui vitae laudantium. Debitis et dolorum quas eius quod.</p><p>Omnis optio ut consequatur omnis voluptatibus aliquid dolore. Possimus et assumenda ducimus ab quibusdam ad. Similique est sit quo laborum. Tempore quia quia at voluptates.</p><p>Et enim ducimus nemo similique qui debitis sed debitis. Saepe debitis laudantium fugit. Sit eligendi nulla reiciendis autem et asperiores non.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(6, 1, 5, 'Fugiat ipsam omnis dolores in accusantium.', 'odio-sint-quo-voluptas-suscipit', NULL, 'Voluptas commodi dignissimos non labore deleniti perspiciatis cum. Et labore unde ipsum numquam reprehenderit enim amet aspernatur. Dolore illo nihil dolor ullam id dolores neque. Ut odit repudiandae voluptas doloremque voluptates. Adipisci deserunt magnam non cum est incidunt.', '<p>Qui culpa est vel. Non qui id et voluptatem sed. Deleniti officia animi ea pariatur ullam debitis atque.</p><p>Voluptatem voluptas autem odio sit inventore dolores odit. Consequatur molestiae doloribus et placeat reiciendis quas. Numquam ipsam aut dolorem qui qui velit. Cum fuga est omnis accusantium cumque sunt voluptas.</p><p>Possimus corporis non praesentium commodi tempore omnis. Assumenda dolorum aut magnam odio officiis non. Magnam autem earum nesciunt ut voluptatem. Voluptatem ut molestiae ullam itaque error.</p><p>Sunt et blanditiis quaerat repellendus fuga maxime aliquam. Sit porro vitae voluptatem laboriosam. Enim voluptas ipsam molestiae ut numquam labore.</p><p>Tempora fugiat odit aut at laboriosam perferendis id ab. Voluptatem inventore optio repudiandae. Quia aut in adipisci aut rerum tempore quasi. Assumenda ipsa officiis rerum sunt mollitia laudantium.</p><p>Neque nam cum molestiae amet. Consectetur dolorem quo quos voluptates. Culpa iste ex ad.</p><p>Nihil incidunt reprehenderit est minima occaecati vel. Libero inventore officia occaecati enim et delectus. Laborum sit possimus repudiandae odio occaecati sunt repellendus. Sunt quod molestiae ut quas dolor eos.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(7, 1, 1, 'Non consequatur adipisci velit necessitatibus et rerum.', 'sunt-repellat-officiis-fugiat-et-doloremque-omnis-reiciendis', NULL, 'Mollitia modi quia asperiores expedita explicabo magni eveniet exercitationem. Iure qui dolore numquam deleniti enim eveniet. Quasi architecto aut fugiat sint repellendus itaque.', '<p>Velit quas consequatur voluptatum ea. Ut nulla at voluptatibus voluptas ut. Sed ex cupiditate commodi voluptate unde itaque qui tenetur. Dicta et necessitatibus et eos delectus quasi rerum et.</p><p>Sed error nesciunt ut vel ea rem. Dignissimos voluptatum sit ipsa totam perferendis molestiae adipisci. Incidunt voluptatem tempora totam deleniti aut voluptates deserunt.</p><p>Earum repellat quaerat et. Dignissimos est consequatur sint voluptatem maiores perferendis laudantium. Voluptatum voluptatem velit in fugit libero iste non voluptate. Molestiae necessitatibus nobis ab et.</p><p>Dolorem necessitatibus quia rem nemo repellat exercitationem. Rerum vero qui minima explicabo voluptas aut ut. Est esse ipsum omnis magnam a dolores dolorum in.</p><p>Quasi earum eius dicta aliquid illo non molestias. Enim id aut deserunt ipsum. Fugiat est ea sed. Soluta ut quasi maiores sit at totam maiores. Eum nemo in minima voluptatem dolorum voluptas ducimus.</p><p>Eos dolorem est et porro iure cumque ratione. Iusto rerum quaerat cumque culpa culpa vero. Labore quisquam laudantium et quam commodi.</p><p>Quia ab sequi exercitationem suscipit laborum. Omnis saepe consectetur omnis dolores autem reprehenderit ex perferendis. Aut veniam nihil consequatur delectus nihil sit.</p><p>Reprehenderit molestiae cupiditate nobis alias quis. Ea perferendis et sequi rerum esse. In sint magni officia qui distinctio.</p><p>Deserunt ut qui cum quas qui aliquam. Eos qui quia ex sed. Et et dolor veniam culpa assumenda nulla ipsa. In qui neque a et est.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(8, 1, 2, 'Iure voluptas sed vero rerum et ut rerum.', 'soluta-ratione-temporibus-reiciendis-voluptatem', NULL, 'Voluptates officia quo aut laboriosam in. Aut molestias debitis ab ex. Excepturi voluptatem corporis et sit ea corrupti totam. Ab architecto commodi quas reprehenderit non eos.', '<p>Maxime distinctio nihil nobis dolor. Dicta et magnam vel aut.</p><p>Sapiente sed sit eveniet consequatur ut labore vel asperiores. Nam est et repellendus aut provident molestiae. Molestias fuga dolore sit vel saepe earum.</p><p>Non omnis doloribus provident ducimus. Illo temporibus voluptas velit quod commodi. Dolore fugit molestias adipisci est. Temporibus ipsum sed quia unde distinctio. Repellat debitis ut aperiam aut sunt.</p><p>Consectetur totam vel corrupti et sint deserunt. Maiores voluptas dolorem qui quisquam ea blanditiis qui. Corporis asperiores sunt consequuntur officia velit quia sunt. Et et quo labore eos veniam et eos.</p><p>Et dolores qui enim quo. Culpa dicta non qui consectetur. Atque recusandae laudantium aut omnis numquam. Ut libero aut incidunt qui sapiente dolorem.</p><p>Repellat saepe et qui vel earum ut non. Id qui in a veniam nemo aliquam odio. Ea et similique eum soluta veniam rerum.</p><p>Officia excepturi illum eos voluptas saepe veniam explicabo. Ratione ea dicta sit rerum eum non ullam. Id modi et odio beatae eos. Tenetur beatae placeat vero est a minima beatae.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(9, 3, 2, 'Et quibusdam assumenda nesciunt sed.', 'ex-magni-quis-iure-tempore-qui-veniam-optio', NULL, 'Culpa cumque commodi non earum sint sint eum. Possimus aut error sed odio vitae voluptas reprehenderit. Qui iure quos voluptate reprehenderit repellat similique id.', '<p>Dolorum molestias iusto qui provident laboriosam quo iusto. Voluptatem dolores excepturi laborum non et recusandae qui. Repellat quia repellendus laboriosam odio aperiam asperiores. Consequatur fugit quasi qui exercitationem occaecati est magnam.</p><p>Quia amet ut porro cupiditate. Aut officiis voluptate nam beatae. Vitae dolores numquam rerum exercitationem debitis debitis quae nam. Nesciunt aut occaecati asperiores.</p><p>Consequatur qui fugit est dolores. Reiciendis deleniti vero asperiores sed possimus consequatur qui. Soluta fuga cumque omnis doloribus neque. Aut vero voluptatem enim.</p><p>Itaque quasi veniam aperiam doloremque impedit. Deleniti aspernatur magnam eaque maiores et. Esse consequatur esse quia inventore.</p><p>Minus occaecati deleniti libero. Facilis voluptatem est eum quos cumque voluptas. Quos ducimus id aperiam quasi corporis. Adipisci dolor adipisci est qui.</p><p>Provident porro ut iusto provident. Debitis aliquid ut distinctio. Molestiae sed debitis sint sed.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(10, 4, 4, 'Saepe eos alias.', 'cum-aut-sed-eos-ipsum-facere-quae', NULL, 'Officiis placeat molestiae facere odio repellat dolorem perspiciatis. Enim qui dicta soluta rem veritatis. Ad voluptas sapiente reiciendis qui vero voluptatem. Sapiente veritatis non accusamus nostrum minima illum repellendus cupiditate.', '<p>Ut sint consequatur ad culpa. Animi placeat aliquid harum ducimus necessitatibus voluptas.</p><p>Ullam sed aliquam assumenda cum et. Autem occaecati aut delectus tempore. Ut excepturi doloremque molestiae. Voluptatum sunt veniam porro a quam voluptatem suscipit.</p><p>Magnam nesciunt soluta blanditiis non. Rerum aut veritatis natus eligendi repellat.</p><p>Suscipit voluptas libero dolorem repellat nihil sunt. Blanditiis voluptas hic dolores aspernatur magnam esse. Totam quaerat ducimus facilis nemo est.</p><p>Omnis tempore occaecati et perferendis officia quas sint. Sed et accusamus libero dolorem. Sit assumenda ullam totam voluptates id praesentium ab quam.</p><p>Autem nesciunt accusamus officia sit optio. At corporis nemo libero error. Quo rerum reiciendis similique minima excepturi ut quia. Quia rerum enim quo earum ut et est et.</p><p>Veritatis ut eaque sit voluptatum quisquam autem. Est expedita eum incidunt laudantium explicabo omnis soluta. Consequuntur amet facilis debitis repellendus sunt nulla doloribus. Voluptatem libero at quia quo occaecati.</p><p>Quisquam tenetur ipsa alias iste. Assumenda dolores culpa consectetur laudantium. Et qui accusantium est.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(11, 4, 1, 'Quis incidunt voluptatem vel autem et.', 'eum-magni-neque-ut-aliquam-fugiat-vitae', NULL, 'Non qui ut nisi totam quo expedita nihil. Aut dolor atque odit molestias eligendi saepe quasi.', '<p>Ut consequatur laboriosam eaque dolor est. Doloribus eligendi nulla odit ut saepe magnam sed sed. Quas sunt voluptatem nemo facere.</p><p>Incidunt minima eius at praesentium quam reiciendis magnam a. Quas repudiandae voluptatem ratione doloribus at. Veritatis aut laudantium aspernatur eligendi.</p><p>Repudiandae amet voluptatem et. Quia ut ad aut ut velit totam velit. Maxime et ab nobis culpa odio repudiandae dolores.</p><p>Itaque quibusdam est excepturi rem praesentium. Repellendus cum beatae et voluptatem non voluptas suscipit. Omnis explicabo architecto occaecati cupiditate et eum. Dolore eligendi natus eveniet ullam sed perspiciatis.</p><p>Sed dolore facere soluta ut non. Unde commodi quis eius nostrum distinctio libero. Aut totam non non eos. Rerum cumque illum officia quo rerum veniam temporibus. Quo ipsam accusamus corporis nisi necessitatibus.</p><p>Recusandae fugit ea nam eaque voluptatem. Non officiis vitae reprehenderit saepe quisquam repudiandae et. Reprehenderit eos minima ea impedit sunt in quibusdam. Vero unde voluptatibus omnis praesentium voluptate.</p><p>Ducimus quae repudiandae magni atque enim sit suscipit. Ut eum corporis quisquam sit nihil cupiditate qui. Tenetur quia in hic magnam aliquid eos nobis. Consequatur debitis quaerat voluptas qui illo atque.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(12, 3, 1, 'Aut in porro dolorem.', 'doloribus-qui-quis-nostrum-aspernatur', NULL, 'Eius laboriosam qui nihil est. Qui ipsum magnam dolor maiores libero quae vel. Enim fugiat accusantium non consequatur et dolores.', '<p>Aspernatur ipsum similique aut sed minus rerum quisquam. Voluptatum ea molestias iste quia nobis cum odit. Cumque qui dolor fugiat voluptate rem labore.</p><p>Pariatur eum quia labore maxime harum. Reprehenderit dolor aperiam ducimus sequi. Fugit maxime in velit ut.</p><p>Veritatis harum eos sequi laudantium quo enim doloremque. Soluta quibusdam itaque laudantium accusamus est. Nesciunt vel laudantium libero omnis.</p><p>Ullam praesentium voluptas aliquam illo nulla iste. Et ut debitis iste et est ex deleniti. Dolor cum ut quas qui exercitationem cupiditate omnis. Aperiam in alias cupiditate et.</p><p>Officiis perferendis corrupti culpa. Vel eligendi autem nemo. Earum mollitia voluptate ab omnis quia tenetur.</p><p>Aspernatur rerum autem at laboriosam voluptas voluptates reiciendis facilis. Vel inventore possimus et illo et. Odit corporis quasi distinctio ea aut ex est.</p><p>Animi aut reprehenderit omnis molestiae magni ut. Omnis dolorum cumque debitis sit non voluptatem. Eveniet in vero dolorem ipsam expedita veniam sit.</p><p>Nulla et aut rerum. Ullam eos aut impedit expedita. Et aut omnis perspiciatis nisi sit sed.</p><p>Cupiditate culpa voluptate eius ut ducimus molestiae. Culpa adipisci voluptas ut eveniet accusamus quae. Eligendi sunt occaecati dolore reprehenderit error cum.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(13, 4, 4, 'Dolores vitae velit dolorem repellendus velit.', 'non-rem-explicabo-consequatur-omnis', NULL, 'Rerum est facere ea harum voluptatum. Sint molestiae doloribus distinctio voluptatibus deserunt. Praesentium omnis cumque mollitia inventore.', '<p>Ea debitis libero possimus cum quasi et dignissimos. Quasi et optio enim iusto. Tenetur quo fugiat quia aut laborum. Eos et at omnis ea enim eius.</p><p>Voluptatem officia exercitationem aut adipisci repudiandae. Quo maxime neque consequuntur molestiae delectus quisquam quis. Iusto ut architecto odit sequi aut vel nulla quisquam.</p><p>Est esse voluptatem aut asperiores corporis. Id nesciunt magni veritatis est in neque. Aliquid est quo provident totam. Numquam incidunt est omnis aut in voluptas.</p><p>Repudiandae et omnis ut deleniti voluptatem. Soluta illo laborum occaecati laborum sunt itaque voluptatum. Est occaecati quis necessitatibus temporibus. Totam architecto harum mollitia quia dolores dolorem consequatur. Deleniti ut eos repudiandae illo.</p><p>Eum provident labore explicabo suscipit aliquam consequatur. Sit libero eum sunt. Quia reiciendis ea ratione.</p><p>Quidem itaque voluptates est amet ad distinctio. Animi rerum sed aut magni. Eos sunt recusandae odio optio pariatur laboriosam quis.</p><p>Deleniti possimus non possimus sequi. Rem sint nam voluptas ut. Cumque sed quaerat voluptatem modi nesciunt modi.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(14, 4, 1, 'Quidem voluptas corrupti.', 'totam-et-ipsa-et', NULL, 'Sint ducimus in quia beatae veritatis officiis modi. Voluptatibus incidunt est est eum repellat eligendi.', '<p>In nesciunt rerum cum non sunt. Ipsam quos eum ut sunt. Cupiditate ipsa et quia voluptas excepturi tenetur beatae. Eaque est neque qui repellat.</p><p>Qui architecto doloribus et hic a sint explicabo accusamus. Distinctio voluptatem dolores omnis reprehenderit non.</p><p>Recusandae rerum debitis est. Adipisci id qui dolorem voluptatum laborum aut sunt.</p><p>Consequatur voluptate illum rerum earum. Cumque in dolores vero non non molestiae qui. Esse et ipsum veniam fuga. Nostrum veritatis dicta explicabo ullam atque.</p><p>Quibusdam autem culpa est laboriosam enim minima nulla. Qui rerum voluptatibus est dolores exercitationem qui veniam. Quod quaerat veniam excepturi non hic excepturi. Omnis maiores repellat voluptas unde sed et earum.</p><p>Nam et ut maxime odit vel velit. Nisi temporibus ea repudiandae omnis ab omnis sed quia. Accusamus minima temporibus amet iusto sapiente. Dolor libero voluptatum dicta ut.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(15, 1, 3, 'Ab sit molestiae quae dolorem doloribus quia animi.', 'totam-deleniti-culpa-facere-adipisci-fugiat', NULL, 'Impedit animi autem est explicabo qui pariatur. Autem quas iusto adipisci sunt. Veniam sit exercitationem consequatur ullam magni. Laboriosam et occaecati in pariatur totam laboriosam.', '<p>Perspiciatis et et ut quaerat possimus. Totam aspernatur quia non. Nulla sit est ab aspernatur aut.</p><p>Vitae expedita accusantium sunt et. Quasi natus consequuntur natus ipsam nam molestiae. Necessitatibus eius necessitatibus eum autem. Hic dolorem itaque voluptatum in ad aut.</p><p>Aut veniam praesentium aut. Enim error labore mollitia et ut id repellendus dolorem. Tempora quos dolores in saepe voluptates ut modi rerum.</p><p>Rerum ut quidem temporibus sit consequatur est et. Alias expedita rerum velit laudantium est. Accusantium fugit eum cumque est magni sapiente quasi. Tenetur eligendi ut quis voluptatem sapiente.</p><p>Aut quis voluptatem ullam quibusdam quasi omnis autem. Repellat vel nam eum cupiditate aut optio ullam. Quia neque quia incidunt. Non ea et laboriosam quia sed quam tempore nobis.</p><p>Aut exercitationem praesentium minima exercitationem ex accusamus. Exercitationem porro sequi ducimus in dolores dicta fugiat quidem. Fugiat qui alias adipisci illo ex. Voluptates enim ipsum fugit aut.</p><p>Voluptatum non ipsa qui facilis nihil. Voluptatem beatae et debitis harum. Eius optio non voluptatum.</p><p>Et nam vel aspernatur facilis. Pariatur sed labore consequatur quae. Sed explicabo sequi repudiandae qui. Nam dolor voluptatem laborum quis reprehenderit et nihil.</p><p>Nihil quia est eum sapiente esse qui ut. Sunt porro ut enim aut et. Modi minima et quidem exercitationem in quibusdam velit. Quis et atque optio. Eos magni est illo quod.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(16, 4, 3, 'Iusto aut corporis aut optio asperiores totam.', 'porro-molestiae-nam-in-cum-et-saepe', NULL, 'Unde est est est et non dolorum laudantium deleniti. Fugit molestiae quas qui architecto voluptates commodi deleniti. Iusto et voluptatem animi enim quasi error consequatur. Id temporibus sapiente a facere id accusantium accusantium.', '<p>Consequatur illo sit unde eum dolor perspiciatis eius. Voluptatem eum et ut quo odit est. At repudiandae laborum dolor doloribus quia. Voluptate necessitatibus eius repudiandae quasi esse saepe autem.</p><p>Ex rerum et quo velit ullam. Ex possimus impedit consequatur voluptates enim in enim. Ut numquam possimus repudiandae nobis atque. Placeat officia iste inventore libero laudantium dolorem quibusdam. Temporibus voluptatem omnis deserunt illo sunt nulla et et.</p><p>Quia autem quibusdam molestiae nobis mollitia molestiae sed. Inventore in distinctio rem odit. Voluptate voluptate eveniet aut fugit.</p><p>Aperiam et perferendis aut. Excepturi voluptatem qui beatae numquam dolor est nam voluptatibus.</p><p>Rem doloribus autem quaerat et sed sint et. Quia illum nihil quidem in quia. In rerum ipsum tempora commodi non quos.</p><p>Nihil repudiandae et aspernatur officia qui quos dolorum magnam. Dicta ea voluptatem tempora tempore. Rerum omnis adipisci consequatur et velit provident. Quod eveniet sed temporibus dolores neque fugit ducimus.</p><p>Animi eos similique in quas doloribus voluptates vero blanditiis. Saepe omnis sit minus et. Itaque adipisci quasi voluptas delectus quis nisi quae.</p><p>Ut id sit illo. Ullam illum sequi sit sed illum voluptas hic. Tenetur labore et ut quaerat et temporibus magnam. Et doloribus cupiditate exercitationem vitae.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(17, 3, 3, 'Sed accusantium corrupti non quos inventore.', 'in-vel-autem-sit-quas-maiores-nostrum', NULL, 'In temporibus ipsum aut quis aspernatur nam aut. Ut est reprehenderit repellat voluptatem excepturi vero. Fugit velit nulla quam fugiat in aut adipisci.', '<p>Impedit omnis iure praesentium dolor. Magni ea voluptas quod possimus quae voluptatem. Non vitae expedita temporibus voluptatibus officiis eum suscipit. Unde sint iure consequatur veritatis laudantium.</p><p>Ut ut et sint. Consequatur libero voluptates earum voluptas voluptatem et eos. Alias rerum tempora aspernatur quam distinctio eius error. Exercitationem fugit numquam accusantium consectetur aut voluptatem sit.</p><p>Officiis minus provident aut alias. Asperiores earum qui quibusdam asperiores inventore. Officia quia vel maiores iste. Et neque sit aut voluptas cum et nulla in. Ut at natus et et.</p><p>Ea nesciunt quod ex eveniet quia deleniti. Mollitia unde repellat aut aut placeat. Est illum ipsam ipsa qui.</p><p>Amet placeat unde non ut doloribus ut. Aspernatur nihil omnis ad itaque reiciendis. Ipsum ut debitis magnam facilis asperiores consectetur.</p><p>Ipsa rerum neque libero aut. Sint deserunt quo quas officia. Perspiciatis fugit molestiae et qui quo sunt. Rerum accusantium fugit maiores natus odio sed veritatis voluptates.</p><p>Ducimus harum soluta aut commodi quis. Omnis voluptatem sit consequatur voluptatem neque.</p><p>Maxime ut voluptatem aliquam omnis neque. Placeat rem in eum dignissimos unde laborum. Dolorum harum voluptates dolores sed facere facere. Reprehenderit quia consequuntur molestiae ut.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(18, 1, 3, 'Et magni.', 'rem-velit-quas-et-ab-eum', NULL, 'Id eum tempora consequuntur delectus totam. Saepe sint et iusto quae fugit. Qui dolore consequuntur molestiae est neque.', '<p>Quis rerum minima ea numquam enim accusamus. Est laborum architecto consequuntur. Perspiciatis qui hic commodi sed et alias et.</p><p>Consequatur sed odio inventore ipsam dolorum. Quas eligendi vel et sunt. Voluptatem ipsa impedit quia aut.</p><p>Laudantium labore rerum eum et atque dolorum. Aut voluptatem voluptates quo. Impedit deserunt laudantium cupiditate tenetur expedita quis. Dolore quis corporis qui commodi sed sit ratione voluptates.</p><p>Sit voluptatem molestiae quis beatae exercitationem. Excepturi nihil nobis quae est. Neque sapiente pariatur ut suscipit culpa expedita.</p><p>Quidem voluptatem consequatur rem. Sit id est placeat qui.</p><p>Perferendis et nobis neque id quasi maxime cum. Dignissimos quia totam qui asperiores reiciendis. Laboriosam ut labore eius perferendis dolorem laboriosam dolorem. Corrupti dolor porro recusandae distinctio inventore sed ad.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(19, 2, 2, 'Nobis et iste blanditiis.', 'explicabo-inventore-architecto-voluptas-voluptatibus', NULL, 'Et incidunt alias blanditiis est. Vel facilis ex quo reiciendis iure sed. Ut blanditiis inventore ipsum. Ex est sed dolorem deleniti ut qui aut sed.', '<p>Quod nulla eius molestiae rerum voluptatibus. Eos aspernatur repellendus id quos possimus. Vel ut repudiandae ut vel dicta. Aut quidem quis consequatur corporis tenetur et. Voluptatem ducimus mollitia officiis est voluptatum.</p><p>Et aut ut dolores animi reiciendis ratione. Consectetur ipsam non optio iure error. Accusamus eius quo atque voluptatem ea amet dolor.</p><p>Facere repellendus nulla reiciendis ullam. Voluptas pariatur sed ea illum nulla suscipit veniam nobis. Eos aliquam ipsam ad dicta repellendus eum.</p><p>Quas voluptate et modi fugiat mollitia quos at. Officiis aut minima consequatur totam ut amet. Dolorem praesentium eligendi ducimus eveniet repellendus voluptate. Ipsam magnam placeat saepe ipsum cum odio atque fugiat.</p><p>Occaecati dolorum numquam labore quasi voluptates. Sint cumque occaecati dolor molestiae dolorem molestiae. At porro dolorem dolorem odit ullam assumenda. Quidem voluptatem saepe sit laudantium expedita et deleniti. Earum magnam inventore at voluptas mollitia nisi.</p><p>Animi dignissimos incidunt dolore earum iste ex. Recusandae odio temporibus consequatur impedit vel. Nihil odit illo aspernatur vel praesentium quia.</p><p>Deserunt qui qui autem accusantium tempora. Veritatis consequatur sint eveniet optio libero et.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(20, 3, 4, 'Nostrum consequatur aliquam eaque atque.', 'est-et-quo-voluptatem-nihil-porro-dicta-nihil', NULL, 'Rerum aliquam dolorem dolores in delectus. Vitae debitis sit aut officiis omnis illo impedit. Quis natus consequatur maxime totam soluta.', '<p>Ut quia fugiat unde velit. Debitis in occaecati voluptas esse sunt ea. Sed aut voluptas et iusto numquam accusantium consequatur. Delectus rerum dolor fugit fugit quis quo minus.</p><p>Corrupti dolor sint laborum rerum est. Reiciendis aut quasi aut aut. Amet exercitationem aperiam dolorem hic sed culpa perferendis dignissimos. Explicabo id iste dolor unde aut fuga voluptates.</p><p>Necessitatibus molestias vero qui iure ipsum. Et veritatis et natus aut est eligendi eligendi. Libero odit enim qui odit qui repellat.</p><p>Officia sed neque qui cumque. Animi vitae impedit consectetur quae. Alias sed voluptatem ut inventore sunt.</p><p>Incidunt ullam vel aliquid illum deserunt reiciendis ad. Sed sit sint ut est earum aperiam ea odit. Reprehenderit voluptas ipsam veniam beatae facere qui. Quibusdam consequatur aut consequatur nisi.</p><p>Itaque dolor illum sapiente commodi quia qui qui illum. Et atque magnam nam eius quam et. Nostrum quia voluptas modi omnis. Reiciendis et error laboriosam beatae quia omnis incidunt.</p><p>Et molestias ut asperiores deserunt ea perspiciatis aut ipsam. Corrupti eveniet architecto qui incidunt repellat molestias nam. Consequatur inventore voluptatum incidunt dolores. Deserunt veritatis unde voluptatem quasi.</p><p>Consequatur voluptas et ut quas asperiores ipsam non. Ut hic laboriosam inventore ipsam repellat. Illo excepturi dolorem porro qui aut quia illum.</p><p>Nihil fugiat modi perferendis blanditiis repellat. Est sed consequuntur rerum debitis ipsa veniam. Sapiente eum et magni impedit quia distinctio.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(21, 1, 4, 'Unde eos voluptate neque veniam tenetur.', 'iure-repellat-sed-sit-distinctio', NULL, 'Aut at molestiae pariatur molestiae nam quae sapiente. Molestiae quibusdam aut quia sit dignissimos cum id ut. Cum totam necessitatibus aliquam minima ea voluptate eaque et. Quibusdam consequuntur velit ab neque.', '<p>Ut cumque et est enim. Incidunt iure aut autem facere.</p><p>Sed voluptatem ut ut. Doloribus quia molestias nesciunt eligendi nisi. Corporis illo voluptatem omnis. Dicta eos aliquam quia voluptatem enim perspiciatis exercitationem.</p><p>Ad voluptas rem saepe nemo accusantium modi pariatur. Delectus minima dolorem aut. Quidem deserunt rerum magnam sapiente illo possimus. Blanditiis sit possimus voluptatibus officiis. Expedita a corporis cum quam non.</p><p>Sit id omnis voluptas dolore. Et cumque dolor molestiae unde consequatur fugiat.</p><p>Quisquam quo esse qui aut architecto nesciunt optio et. Facilis sit ab qui quaerat amet nisi. Et nisi aut delectus eum. Tempora quos possimus esse consequatur iste. Reiciendis aliquid cum earum quidem similique voluptatem sit iusto.</p><p>Ipsa itaque nam repellat facilis. Fugit illo beatae impedit aut sed.</p><p>Accusamus fuga qui reiciendis. Quas at itaque quis illum cupiditate repellat non. Vitae facilis aut harum corporis veritatis.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(22, 3, 3, 'Aspernatur consequuntur sit saepe voluptatum.', 'vel-aut-odio-voluptatibus-est-occaecati-et-et', NULL, 'Aut officia eligendi eaque officia est corporis. Quas consequuntur qui soluta aspernatur sint quasi fuga est. Repellat ut et et iusto nobis iusto iusto. Numquam sit itaque maxime.', '<p>Dolor nihil possimus consectetur est ut. Nobis sunt porro animi. Alias eligendi omnis esse magnam aut doloremque deserunt.</p><p>Unde magnam ut perspiciatis similique. Eius voluptas reprehenderit doloremque et. Pariatur illum cupiditate possimus ut. Magnam sed quae non voluptatum magnam et accusantium. Optio nulla laboriosam vel et quas sed.</p><p>Aut consectetur modi possimus aliquam magnam repellendus consequatur. Ea quaerat autem dolores id. Repellendus magnam error dicta est qui. Dolor sapiente laboriosam qui enim qui.</p><p>Quisquam similique mollitia voluptas et. Voluptates doloribus autem quasi voluptas et.</p><p>Ipsa repudiandae omnis sunt totam similique distinctio. Accusamus eum sit dicta libero aspernatur. At qui ratione dolores eveniet. Culpa quisquam qui modi soluta adipisci consequatur totam id.</p><p>Dolor et ut ut laboriosam unde voluptatem et. Et aut quia molestias corrupti est eveniet consequuntur. Mollitia a provident voluptatibus et atque. Blanditiis totam et et eaque. Rerum in qui blanditiis laborum itaque sequi quasi.</p><p>Vitae ut ut est dolorem qui ut. Nisi hic temporibus iusto qui consequatur autem. Maiores beatae aliquid non. Quos eveniet quasi nemo nulla rerum molestiae.</p><p>Voluptatibus rerum corporis ipsam non id quidem est. Autem aut atque reprehenderit labore pariatur. Adipisci voluptate nemo labore repudiandae.</p><p>Voluptas voluptas velit sunt ipsa eius. Asperiores vel facere vitae nam dicta ut repellendus. Molestias modi minus et nemo.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(23, 3, 1, 'Commodi non placeat nam sunt vitae ut ab autem consequuntur.', 'consequatur-et-maxime-non-sunt', NULL, 'Voluptates explicabo quibusdam cumque sed. Quae magni quis culpa. Minus reprehenderit eos vel nostrum. Est ea ipsam cumque illum quisquam.', '<p>Ea consectetur corrupti quaerat aut illo. Molestiae voluptatum et nihil id perspiciatis ad. Magnam magni quisquam tempora sed consequatur.</p><p>Eum sint laborum quo dolores autem delectus dolores. Quam beatae iusto ipsam illo quidem eius. Dolore sed ut eos hic. Voluptates earum rerum animi maxime consectetur temporibus.</p><p>Quia voluptas unde aut eos a ut. Minus ab impedit aut labore.</p><p>Numquam totam nostrum molestiae rerum doloribus officia et ut. Accusamus aut cumque cumque sed maiores labore enim. Aliquam libero velit cumque iste alias sint.</p><p>Aut est dignissimos enim optio. Eos harum recusandae non at. Ullam laboriosam voluptas aut quas ut vitae. Reprehenderit esse laudantium cumque id ea rerum.</p><p>Totam possimus temporibus numquam nesciunt aut excepturi velit. A blanditiis sed delectus maxime at molestias. Suscipit voluptas consequatur omnis qui ea laborum accusantium. Architecto eum placeat sunt.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(24, 2, 2, 'Cum maxime doloribus excepturi laudantium ut qui.', 'odio-sequi-molestiae-sit-enim-est', NULL, 'Rem sit est id facilis. Sed odit ipsa et et nostrum mollitia. Labore quaerat qui sed. Autem perferendis dolor rerum dignissimos voluptas.', '<p>Et ducimus ut cupiditate quae reiciendis vel sed. Ex dolorum omnis incidunt sequi dicta fugit. Et aut consequatur consectetur est. Dolorem enim voluptas qui quia excepturi.</p><p>Dolores ipsa rerum veniam. Minima officiis et pariatur sed. Eaque sit est quis. Est in deserunt eligendi et quia recusandae et.</p><p>Et illo quia corporis molestiae ea sit. Facere qui vero assumenda. Voluptatibus accusantium ut molestiae ex.</p><p>Perspiciatis cumque fugit qui sed pariatur repellendus. Harum repellat modi id cum. Aut officiis veniam quos voluptatem consequuntur. Quia eveniet voluptate quod in eius.</p><p>Voluptatem et culpa debitis cumque aspernatur quibusdam. Sequi magnam enim quisquam consequatur. Et natus incidunt dicta laboriosam voluptas. Ab accusamus et eum et possimus libero assumenda. Aut reprehenderit qui consequatur aut qui.</p><p>Sint porro minima vel nesciunt. Aperiam beatae rerum rerum neque dolores aspernatur. Rem quam odio aut non. Et magni exercitationem et natus.</p><p>Autem a vero porro aut corrupti adipisci rem. Illum odio quia est quibusdam officia culpa. Soluta sed voluptatum quos ex nihil voluptas. Exercitationem temporibus eum repellat.</p><p>At et qui itaque inventore quos est ut. Libero magni perspiciatis ex quod nihil magnam voluptas.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(25, 2, 1, 'Doloremque rerum accusantium libero consequuntur consequatur quasi cum.', 'dolor-vel-quod-aut-placeat', NULL, 'Non saepe esse voluptas praesentium sapiente eum porro qui. Dolore asperiores quo officia modi architecto. Ipsa dolor quae aut necessitatibus.', '<p>Nesciunt et nulla consequatur quia et asperiores. Nisi et sit porro excepturi laudantium et. Rerum ea sint aut nisi voluptatibus id eum. Fuga culpa odit eum maxime aut accusamus. Sint quis et assumenda quia.</p><p>Dolores voluptatem facilis accusantium dolores sunt illo sed. Ut ad eos odit voluptatum id nemo.</p><p>Nesciunt eligendi est quis natus nihil debitis illum. Molestiae qui est dolorem quae autem qui sit. Doloremque saepe magni est sapiente officiis nihil sed.</p><p>Aut sapiente tempora autem sint. Magnam similique impedit ducimus vel fugit. Dolorem accusantium nobis cumque omnis optio sint porro magnam. Autem culpa id iure voluptatibus autem similique recusandae.</p><p>Sed illum magnam quasi quia. Vel molestiae temporibus quia nihil molestiae eius maxime. Minus nemo et velit repellat mollitia praesentium est. Magni ut facilis dolores vero sint illo sit.</p>', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Arif yudi', 'Arif yudi s', 'yudi17564@gmail.com', NULL, '$2y$10$znNR552axdI.nhdI2GUA/e2kuYfDoL0rHtwLTDhHfy6lh3DKWYZ6q', NULL, '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(2, 'Alek Macejkovic III', 'xwolff', 'rebecca63@example.net', '2022-09-16 16:46:30', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NmyK1UwlCN', '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(3, 'Charlotte Christiansen', 'marta99', 'beth.vandervort@example.com', '2022-09-16 16:46:30', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bLIzmNnvfN', '2022-09-16 16:46:30', '2022-09-16 16:46:30'),
(4, 'Florine Weissnat', 'stanton.name', 'araceli12@example.net', '2022-09-16 16:46:30', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LMN20ll3MA', '2022-09-16 16:46:30', '2022-09-16 16:46:30');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
