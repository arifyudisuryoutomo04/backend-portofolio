<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\categories;
use App\Models\posts;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        User::create([
            'name' => 'Arif yudi',
            'username' => 'Arif yudi s',
            'email' => 'yudi17564@gmail.com',
            'password' => bcrypt('password')
        ]);

        User::factory(3)->create();

        Categories::create([
            'name' => 'Experience',
            'slug' => 'experience.'
        ]);
        Categories::create([
            'name' => 'Portfolio',
            'slug' => 'portfolio'
        ]);
        Categories::create([
            'name' => 'Achivement',
            'slug' => 'achivement'
        ]);
        Categories::create([
            'name' => 'Activity',
            'slug' => 'activity'
        ]);
        Categories::create([
            'name' => 'Blog',
            'slug' => 'blog'
        ]);

        Posts::factory(25)->create();

    }
}
